import numpy as np
import warnings
import csv
import pandas as pd
import matplotlib.pyplot as plt

# -----------------------------------------------------------------------------------------------------------------------
# Usage:
# day = allows you to select what days to observe (mondays, tuesdays, wednesdays, thursdays, fridays, saturdays, sundays)
#
# coefficients = plots the regression line given x and y variables with degree of d (in the code atm, 3 is the degree);
# mapping it to the maximum possible will cause the regression line to fit the data set 1:1 - connecting the dots)
# ----------------------------------------------------------------------------------------------------------------------
# The defined function tests and finds the maximum degree given the csvs supplied; a bit redundant at this point,
# but may be used in the future?
# -----------------------------------------------------------------------------------------------------------------------

# def polyfitRegression(x,y,d):

#    for d in range(0, 100):
#       p = np.polyfit(x,y,d)
#      with warnings.catch_warnings():
#         warnings.filterwarnings('error')
#        try:
#           coefficients = np.polyfit(x,y, d)
#      except np.RankWarning:
#         print ("not enough data")
#        break
# print(p)


# Change to what day you want to check (monday,tuesday,wednesday,thursday)
day = "sundays"
# Degree used in regression (1 = linear, 2 = quadratic, 3 = cubic etc)
deg = 3

# extracting number of rows in csv
with open("../Inputs/" + day + "OrderCounts.csv", "r") as f:
    reader = csv.reader(f, delimiter=",", quotechar='"', skipinitialspace=True)
    data = list(reader)
    row_count = len(data)

# extracting  order count per listed day
delCount = pd.read_csv('../Inputs/' + day + "OrderCounts.csv", dtype=None)

# Calculating x and y variables used in plotting
x = range(0, row_count - 1)
y = delCount['deliveryCount']

# for d in range(0, 100):
#    p = np.polyfit(x, y, d)
#    with warnings.catch_warnings():
#        warnings.filterwarnings('error')
#        try:
#            coefficients = np.polyfit(x, y, d)
#        except np.RankWarning:
#            print ("not enough data")
#            break
#    print(p)
#    deg = d

# Fit a polynomial p(x) = p[0] * x**deg + ... + p[deg] of degree deg to points (x, y).
# Returns a vector of coefficients p that minimises the squared error.
# Plot the polyfit line using a given degree (x-axis,y-axis,degree)
coefficients = np.polyfit(x, y, 3)
polynomial = np.poly1d(coefficients)
ys = polynomial(x)
yfit = np.polyval(coefficients, x)
# print coefficients
# print polynomial
# print yfit

# graph labels
plt.title("Order counts every " + day)
plt.xlabel("Week")
plt.ylabel("# of orders")
plt.plot(x, y, label='Data')
plt.plot(x, yfit, label='Best fit')
plt.plot(x, yfit - y, label='Residue')
plt.legend(loc='upper right')

# changes the x-axis tick frequency to count every iteration instead of every 2nd one
plt.xticks(np.arange(min(x), max(x) + 1, 1.0))
plt.grid()
plt.show()