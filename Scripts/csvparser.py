#deprecated?

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import csv
import datetime as dt
import unittest

x,y,xticks=[],[],[]
csv_reader = csv.reader('weather_data1.csv')

data = pd.read_csv('weather_data1.csv',delimiter=',',dtype=None)
dates=data['Date']
#converts the data from, the dates column into a format that can be processed
dates = [dt.datetime.strptime(d,'%Y-%m-%d').date() for d in dates]

#print dates
fig, ax = plt.subplots()
#plots the max temp line as red
ax.plot(data['Maximum Temperature (C)'],'r-',label='Max Temp')
#blots the min temp line as blue
ax.plot(data['Minimum Temperature (C)'],'b-',label='Min Temp')
ax.legend(loc=4, fontsize=10)
#Set the dates as the x-axis ticks
ax.set_xticks(np.arange(len(dates)))
ax.set_xticklabels(dates)
plt.xticks(rotation=90)
#set the labels for the axes
ax.set_xlabel('Date')
ax.set_ylabel('Maximum Temperature')
plt.grid()
plt.show()
