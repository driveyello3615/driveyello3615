import matplotlib
import csv

matplotlib.use('pdf')
import matplotlib.pyplot as plt
import seaborn.apionly as sns
import pandas as pd
import functools
import datetime as dt
import numpy as np
import sys
import warnings

# Function: Str type -> Date type (YYYY-MM-DD).
to_date = lambda x: dt.datetime.strptime(x, '%Y-%m-%d').date()

# Get shift_views.
raw_shifts = pd.read_csv('../Inputs/shift_view.csv', dtype=None)
raw_shifts['start'] = raw_shifts['start'].str[:-9].apply(to_date)
# Get weather data.
raw_weather = pd.read_csv('../Inputs/weather_data.csv', dtype=None)
raw_weather['Date'] = raw_weather['Date'].apply(to_date)
# Store locator
store_locator = pd.read_csv('../Inputs/stores_view.csv', dtype=None)
holidays = list(map(to_date, ["2016-04-25", "2016-06-12"]))

"""
#(Static) Logical vector: True if row in NSW, False otherwise.
in_nsw = functools.reduce(
         lambda x, y: x & y,
         [raw_shifts['storeId'] != n for n in [118, 187, 191, 195, 196, 198, 203, 205, 206, 209, 210, 221, 222, 240, 242, 246]])
"""
# (Dynamic) Logical vector: True if row in NSW, False otherwise.
# Have filtered out non-Sydney stores (187, 191, 195, 198, 206, 209, 210, 242)
in_nsw = functools.reduce(
    lambda x, y: x & y,
    [raw_shifts['storeId'] != n for n in
     [s for s in
      store_locator[store_locator['administrative_area_level_1'] != 'NSW']['id']]
     ])

# All shifts in NSW.
all_shifts = raw_shifts[in_nsw]

# All valid shifts in NSW. (C == C iff C is not NaN)
valid_shifts = raw_shifts[in_nsw &
                          (raw_shifts['actualStart'] == raw_shifts['actualStart']) &
                          (raw_shifts['actualEnd'] == raw_shifts['actualEnd']) &
                          (raw_shifts['shiftStateId'] == 6)]

if len(sys.argv) > 1:
    valid_shifts = valid_shifts[(valid_shifts['start'] >= to_date(sys.argv[1])) &
                                (valid_shifts['start'] <= to_date(sys.argv[2]))]

tidy_weather = raw_weather[(raw_weather['Date'] >= valid_shifts['start'].min()) &
                           (raw_weather['Date'] <= valid_shifts['start'].max())]

sydney = tidy_weather[tidy_weather['Location'] == 'Sydney']
sydney_airport = tidy_weather[tidy_weather['Location'] == 'Sydney Airport']


def date_range(df, col):
    """
    Returns incrementing sequence of dates from earliest to latest in df['col'].
    Assert df is a dataframe.
    Assert col is a column name of dates in df.
    """
    s = df[col].min()
    e = df[col].max()
    return [s + dt.timedelta(days=x) for x in range(0, (e - s).days + 1)]


def get_day(d):
    """
    Returns any date as a String in the format DD-MM-YYYY.
    """
    return str(d.day) + '-' + str(d.month) + '-' + str(d.year)


def set_canvas():
    """
    Graphing aesthetics.
    """
    sns.set(font_scale=0.5, style='whitegrid')
    sns.plt.gcf().subplots_adjust(bottom=0.3)
    sns.plt.xticks(rotation=90)


def overlay_weather(dr, station):
    """
    Overlays plane with rainfall, max temp, min temp across dates in dr.
    Assert dr is a list od Date types.
    """
    # Rainfall.
    sns.pointplot(x='Date', y='Rainfall (mm)', order=dr,
                  data=station, color='g', scale=0.3)
    # Max Temp.
    sns.pointplot(x='Date', y='Maximum Temperature (C)', order=dr,
                  data=station, color='r', scale=0.3)
    # Min Temp.
    sns.pointplot(x='Date', y='Minimum Temperature (C)', order=dr,
                  data=station, color='b', scale=0.3)


def plot_shiftcounts(n, s):
    """
    Plots actual shifts per date overlaid on all shifts and saves it to s.
    Assert n is a fresh number.
    Assert s is a file name with pdf extension.
    """
    plt.figure(n)
    set_canvas()
    # Incrementing sequence of dates: [earliest, latest].
    dr = date_range(valid_shifts, 'start')
    # Plot count of all shifts per date.
    ax = sns.countplot(x='start', data=all_shifts, color='r', order=dr)
    # Plot count of valid shifts per date.
    sns.countplot(x='start', data=valid_shifts, color='b', order=dr)
    # Add counts to each bar.
    for i, p in enumerate(ax.patches):
        ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x(), p.get_height() + 0.2))
        if i >= len(dr) and dr[i - len(dr)].weekday() == 4:
            p.set_color('g')

    # Set axes labels.
    ax.set(xlabel='Dates', ylabel='No. of Shifts', title='SHIFTS')
    ax.set_xticklabels([get_day(j) if i % 2 else None for (i, j) in enumerate(dr)])
    # Save.
    sns.plt.savefig(s)


def organize_data():
    order_counts = valid_shifts.groupby(['start']).sum().reset_index()
    # Save dr and order counts to a csv file called 'orderCounts.csv'. FIX LATER TO USE PANDAS
    headers = ['Date', 'Deliveries', 'Minimum Temp (C)', 'Maximum Temp (C)', 'Rainfall(mm)', 'Evaporation (mm)',
               'Sunshine (hours)', 'Speed of max wind gust (km/h)']

    toCsv = list(
        zip(map(str, valid_shifts['start']), order_counts['deliveryCount'], tidy_weather['Minimum Temperature (C)'],
            tidy_weather['Maximum Temperature (C)'], tidy_weather['Rainfall (mm)'],
            tidy_weather['Evaporation (mm)'], tidy_weather['Sunshine (hours)'],
            tidy_weather['Speed of Maximum Wind Gust (km/h)']))

    with open('../Inputs/orderCounts.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(headers)
        writer.writerows(toCsv)

    weekdaysList = []
    dataFrame = []
    datesResevoir = []
    headers = ["mondays", "tuesdays", "wednesdays", "thursdays", "fridays", "saturdays", "sundays"]

    # dataFrame = a dataFrame list to give to the pandas csv_writer.
    # weekdaysList is a list by weekdays. weekdaysList[0] = list of all mondays. weekdaysList[1] = list of all tuesdays and so on...
    # weeksList is a list by weeks. weeksList[0] = list of orders in week 1, weeksList[1] = list of orders in week 2 and so on...

    for i in range(1, 8):
        dataFrame.append(order_counts[order_counts['start'].apply(lambda x: x.isoweekday()) == i])
        weekdaysList.append(list(zip(range(len(dataFrame[i - 1]['start'])),
                                     dataFrame[i - 1]['deliveryCount'])))
        datesResevoir.append(dataFrame[i - 1]['start'])
    weeksList = list(map(list, zip(*sorted(weekdaysList))))

    initDate = datesResevoir[0][0]

    # Traverse a list of weeks and append sublists of size n (n is input()). For example, if n = 3, then:
    # data = [[Week 1, Week 2, Week 3], [Week 2, Week 3, Week4], [Week 3, Week 4, Week5], ... ,[Week x - 2, Week x - 1, Week x]]
    n = 10
    data = []
    for i in range(len(weeksList) - n):
        appendable = []
        # print("-------------------------------------------------------------------------------------------------------")
        for j in range(n):
            # print("Week:", i + j + 1)
            # print(weeksList[i + j])
            appendable.append(weeksList[i + j])
        data.append(appendable)

    nDictTraining = {}
    nDictTest = {}
    init = 5
    df = 3
    b = 3

    # nDict is a dictionary of size n - init, where each key in nDict (which is a number between n and init)
    # is a dictionary of 'training' dictionaries of iterative size n.

    # "training' is another dictionary, each referencing a weekday in the "headers" list above ^^^
    # Each key in 'training' has a list of lists of size n and has dates and orders partitioned. E.g. if n = 3, then:
    # training["mondays"] = [[Monday of week 1, Monday of week 2, Monday of week 3], [Monday of week 2, Monday of week 3, Monday of week 4],...,[Monday of Week x - b - 2, Monday of Week x - b - 1, Monday of Week x - b]]
    # Where b is the size of the test data

    for x in range(init, n + 1):
        training = {}
        test = {}
        for names in headers:
            training[names] = []
        for weekdayArray, names in zip(weekdaysList, headers):
            for i in range(len(weekdayArray) - x):
                appendable = []
                for j in range(x):
                    appendable.append(weekdayArray[i + j])
                training[names].append(appendable)

        for keys in training:
            test[keys] = training[keys][-b:]

        for keys in training:
            training[keys] = training[keys][:len(training[keys]) - b]

        nDictTraining[x] = training
        nDictTest[x] = test

    # nDictTraining[n]: Get iterations over all weeks partitioned into arrays of size n
    # nDictTraining[n]['monday']: Get all mondays of iterations over weeks of size n
    # nDictTraining[n]['monday'][0]: Get the 1st (AKA the 0th) monday of the mondays of iterations. Paired list [datetime.date, ordercounts]
    # nDictTraining[n]['monday'][0][0]: Get the datetime.date of the 1st (AKA the 0th) monday of iterations

    # print('Train: ', nDictTraining)
    # print('Test:  ', nDictTest)

    # Test accuracy for each dataset
    # d = 3, foir all n
    # Upgrade to use d=1-5
    # build for loop that gets ya to val
    # print(type(nDictTraining))
    # print(len(nDictTraining))

    m = len(dataFrame[0][['start']]['start'])

    deg = 3
    train = [[]]
    test = [[]]
    resultset = {h: [] for h in headers}  # [len(headers)]
    for h in headers:
        arrSetUpper = []
        for i in range(init, n):  # Say i is 3...
            for d in range(1, df):
                arrSetLower = []
                accuracy = []
                for tr in nDictTraining[i][h]:  # For each item in

                    test = np.array(nDictTest[i][h])
                    # print(a[:1])
                    # print(a[:1][0][0])
                    # print(a[:1][0][1])
                    train = np.array(tr)
                    print(train[:, 0])
                    print(train[:, 1])
                    coefficients = []
                    coefficients = np.polyfit(train[:, 0], train[:, 1], d)
                    print("Coef", coefficients)
                    # with warnings.catch_warnings():
                    #     warnings.filterwarnings('error')
                    #     try:
                    #         coefficients = np.polyfit(train[:, 0], train[:, 1], deg)
                    #         print("Coef2",coefficients)
                    #     except np.RankWarning:
                    #         pass
                    polynomial = np.poly1d(coefficients)
                    yfit = np.polyval(coefficients, train[:, 0])

                    print(coefficients)
                    print(polynomial)
                    print(test[:, 0])
                    estimate = []
                    real = []
                    for q in test:
                        for r in q:
                            print("R:", r[0])
                            estimate = np.append(estimate, polynomial(r[0]))
                            real.append(r[1])
                        print(estimate)
                        estimate = np.array(estimate)
                        estimate = np.absolute(estimate)
                        results = estimate - real
                        results = np.absolute(results)
                        results = np.average(results)
                        accuracy.append(results)
                        print("Average error: ", results)
                accuracy = np.average(accuracy)
                # Calculate total accuray
                # train = np.append(train, arr)
                # arrSetLower.append(i,d,polynomial,accuracy)
                # arrSetUpper.append(arrSetLower)
                if not resultset[h] or accuracy < resultset[h][3]:
                    resultset[h] = ([i, d, polynomial, accuracy])

    print("Resultset: ", resultset)
    # for each weekday
    # determine estimated vlu 2 days past range of data
    # create singlar array and pass to matlibplot
    outputX = []
    outputY = []
    i = 0
    for x in [1, 2]:
        for day in headers:
            # estimate output point based on each regression line
            outputY.append(resultset[day][2](x))
            outputX.append(initDate + dt.timedelta(days=(i)))
            i += 1

    plt.figure(10)
    # ax = plt.subplot(111)
    plt.plot(outputX, outputY, label="Estimation")
    plt.savefig("../images/test.pdf")
    print("End")

    with open('../Inputs/resultSet_opY.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(zip(list(outputX), list(outputY)))

        # writer.writerow(resultset)
        # writer.writerow(resultset.values())

    rs = {}
    # print(train)
    # for i in range(init,n):
    #     #print(i)
    #     #print(nDictTest.get(i))
    #     for h in headers:
    #         #print(h)
    #         arr = []
    #         for a in nDictTest.get(i).get(h):
    #             #print(a[:1])
    #
    #             arr.append(a[:1][0][1])
    #         test.append(arr)
    # print(test)
    # Count per set varies from 13 to 6 (instead of 6 to 10)

    # coefficients = np.polyfit(a[:1], y, deg)
    # polynomial = np.poly1d(coefficients)
    # yfit = np.polyval(coefficients, x)

    # [n][Day][Dates,Value]




    for x, days in zip(dataFrame, headers):
        x[['start', 'deliveryCount']].to_csv('../Inputs/' + days + 'OrderCounts.csv')


def plot_ordercounts(n, s, station):
    """
    Plots count of orders per date and saves it to s.
    Assert n is a fresh number.
    Assert s is a file name with pdf extension.
    """
    plt.figure(n)
    set_canvas()
    # Incrementing sequence of dates [earliest, latest].
    dr = date_range(valid_shifts, 'start')
    # Aggregate all orders by date.
    # (Index must be reset as 'start' is auto-set to index and no longer a column.)
    order_counts = valid_shifts.groupby(['start']).sum().reset_index()

    # Plot count of orders per date.
    ax = sns.barplot(x='start', y='deliveryCount',
                     data=order_counts, color='w', order=dr)

    # Add counts to each bar.
    for p in ax.patches:
        ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x(), p.get_height() + 0.2))

    # Weather.
    overlay_weather(dr, station)
    # Set axes labels.
    ax.set(xlabel='Dates', ylabel='No. of Orders', title='ORDERS')
    ax.set_xticklabels([get_day(j) if i % 2 else ' ' for (i, j) in enumerate(dr)])
    # Limit y-axis range to non-negative values.
    # (Rainfall levels are near enough to 0 to auto-set lower bound to -20.)
    ax.axes.set_ylim(0, )
    # Save.
    sns.plt.savefig(s)


if __name__ == "__main__":
    plot_shiftcounts(1, '../images/shift_counts.pdf')
    plot_ordercounts(2, '../images/order_counts_obs.pdf', sydney)
    # plot_ordercounts(3, '../images/order_counts_airport.pdf', sydney_airport)

    # Replace '3' with input() later
    organize_data()
