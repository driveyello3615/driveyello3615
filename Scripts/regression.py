import numpy as np
import sys
import csv
import pandas as pd
import matplotlib.pyplot as plt
from types import *


# -----------------------------------------------------------------------------------------------------------------------
# Usage:
# Run from command line
#       "python regression.py <day> <degree> <weeks>"
#  <day>    - Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday
#  <degree> - specify the degree of regression. 1 - liner, 2 - quadratic, 3 - cubic, etc
#  <weeks>  - specify the number of weeks to look in advance.
#             If the data set has 18 weeks for example, and we specified <week> as 3, then the regression will map to 21 weeks.
#             Note that beyond two weeks, the regression line will dramatically become unreliable
# ----------------------------------------------------------------------------------------------------------------------
# Variables:
# day = allows you to select what days to observe (mondays, tuesdays, wednesdays, thursdays, fridays, saturdays, sundays)
# coefficients = plots the regression line given x and y variables with degree of d (in the code atm, 3 is the degree);
# mapping it to the maximum possible will cause the regression line to fit the data set 1:1 - connecting the dots)
# Comments on non-trivial variables
# ----------------------------------------------------------------------------------------------------------------------
# There is a conditional (commented out) that tests and finds the maximum degree given the csvs supplied; a bit redundant at this point,
# but may be used in the future?
# -----------------------------------------------------------------------------------------------------------------------

# def polyfitRegression(x,y,d):

#    for d in range(0, 100):
#       p = np.polyfit(x,y,d)
#      with warnings.catch_warnings():
#         warnings.filterwarnings('error')
#        try:
#           coefficients = np.polyfit(x,y, d)
#      except np.RankWarning:
#         print ("not enough data")
#        break
# print(p)

# some assert tests, need to implement some more

def degreeValid(degree):
    assert type(degree) is IntType, "degree is not an integer"
    assert (degree <= 0), "Regression can't be less than 0!"


def dayValid(day):
    assert type(day) is StringType, "Day is not a string! %r" % day


# parse the arguments for processing, probably could be optimised and shorter
try:
    if len(sys.argv) != 4:
        print("Invalid number of arguments. Check your inputs match <Day> <Degree> <Prediction>")
        sys.exit()
    if sys.argv[1] == "Sunday":
        day = 'sundays'
    if sys.argv[1] == "Monday":
        day = 'mondays'
    if sys.argv[1] == "Tuesday":
        day = 'tuesdays'
    if sys.argv[1] == "Wednesday":
        day = 'wednesdays'
    if sys.argv[1] == "Thursday":
        day = 'thursdays'
    if sys.argv[1] == "Friday":
        day = 'fridays'
    if sys.argv[1] == "Saturday":
        day = 'saturdays'
    if sys.argv[1] == "sunday":
        day = 'sundays'
    if sys.argv[1] == "monday":
        day = 'mondays'
    if sys.argv[1] == "tuesday":
        day = 'tuesdays'
    if sys.argv[1] == "wednesday":
        day = 'wednesdays'
    if sys.argv[1] == "thursday":
        day = 'thursdays'
    if sys.argv[1] == "friday":
        day = 'fridays'
    if sys.argv[1] == "saturday":
        day = 'saturdays'
    deg = int(sys.argv[2])
    futureWeek = int(sys.argv[3])
except IndexError:
    print("Invalid day, check your filename is valid or your spelling!")
    sys.exit()
except ValueError:
    print("Degree cannot be a negative!")
    sys.exit()

try:
    # extracting number of rows in csv
    with open("../Inputs/" + day + "OrderCounts.csv", "r") as f:
        reader = csv.reader(f, delimiter=",", quotechar='"', skipinitialspace=True)
        data = list(reader)
        row_count = len(data)
except NameError:
    print("Invalid csv file, check the day argument")
    sys.exit()

# extracting  order count per listed day
delCount = pd.read_csv('../Inputs/' + day + "OrderCounts.csv", dtype=None)

# Calculating x and y variables used in plotting
maxWeeks = row_count - 1
xList = range(0, maxWeeks)
x = np.asarray(xList)
y = delCount['deliveryCount']

# defining limits of x-axis
x_min = 0
x_max = maxWeeks + futureWeek - 1

# redundant function to find the max degree
# for d in range(0, 100):
#    p = np.polyfit(x, y, d)
#    with warnings.catch_warnings():
#        warnings.filterwarnings('error')
#        try:
#            coefficients = np.polyfit(x, y, d)
#        except np.RankWarning:
#            print ("not enough data")
#            break
#    print(p)
#    deg = d


# Fit a polynomial p(x) = p[0] * x**deg + ... + p[deg] of degree deg to points (x, y).
# Returns a vector of coefficients p that minimises the squared error.
coefficients = np.polyfit(x, y, deg)
polynomial = np.poly1d(coefficients)
yfit = np.polyval(coefficients, x)

print("==================================================================\n Results:")
print("Coefficients\n" + str(coefficients) + "\n")
print("Polynomial\n" + str(polynomial) + "\n")
print("yfit \n" + str(yfit) + "\n")
print("==================================================================")

# Axis values for prediction
predictionXFit = np.linspace(x_min, x_max)
predictionYFit = polynomial(predictionXFit)

# graph labels
plt.title("Order counts every " + sys.argv[1].title())
plt.xlabel("Week")
plt.ylabel("# of orders")

# plotting known data
plt.plot(x, y, label='Data')
plt.plot(x, yfit, label='Best fit')
plt.plot(x, yfit - y, label='Residue')

resid = yfit - y
print("Resid", resid)

resid = np.array(resid)
print("Resid", resid)
resid = np.absolute(resid)
print("Abs", resid)
resid = np.average(resid)

print("Average", resid)

# prediction plot
plt.plot(predictionXFit, predictionYFit, label='Prediction')

plt.legend(loc='best', numpoints=1)
print("Graph of: " + day.title() + "\nUsing degree of: " + str(deg) + "\nWith a prediction of: " + str(
    futureWeek) + " weeks")

# changes the x-axis tick frequency to count every iteration instead of every 2nd one
plt.xticks(np.arange(x_min, x_max + 1, 1.0))

plt.grid()
# save resulting graph into a png
plt.savefig('../images/RegressionLineFor%s.png' % day.title())
plt.show()
