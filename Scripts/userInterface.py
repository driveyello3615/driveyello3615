#!/usr/bin/python
from tkinter import *

paramList = []

top = Tk()
top.resizable(width=False, height=False)
top.minsize(500, 360)
top.wm_title("Drive Yello Anaylsis")

L0 = Label(text="Specify graph file type:")
L0.place(x=0, y=0)
var = IntVar()
R1 = Radiobutton(top, text="png", variable=var, value=0)
R1.place(x=0, y=20)
R2 = Radiobutton(top, text="pdf", variable=var, value=1)
R2.place(x=60, y=20)

L1 = Label(top, text="Specify file output")
L1.place(x=10, y=40)
E1 = Entry(top, bd=5)

E1.place(x=10, y=60)

L2 = Label(top, text="Specify input file")
L2.place(x=10, y=85)
E2 = Entry(top, bd=5)
E2.place(x=10, y=105)

L3 = Label(top, text="Specify important dates csv location (optional)")
L3.place(x=10, y=130)
E3 = Entry(top, bd=5)
E3.place(x=10, y=150)

L11 = Label(top, text="Outputs:")
L11.place(x=10, y=175)
L12 = Label(top, text="Order Count")
L12.place(x=10, y=195)
CheckVar1 = IntVar()
CheckVar2 = IntVar()
C1 = Checkbutton(top, text="graph", variable=CheckVar1,
                 onvalue=1, offvalue=0)
C2 = Checkbutton(top, text="csv", variable=CheckVar2,
                 onvalue=1, offvalue=0)
C1.place(x=10, y=215)
C2.place(x=80, y=215)
L13 = Label(top, text="Shift Count")
L13.place(x=10, y=235)
CheckVar3 = IntVar()
CheckVar4 = IntVar()
C3 = Checkbutton(top, text="graph", variable=CheckVar3,
                 onvalue=1, offvalue=0)
C4 = Checkbutton(top, text="csv", variable=CheckVar4,
                 onvalue=1, offvalue=0)
C3.place(x=10, y=255)
C4.place(x=80, y=255)
L14 = Label(top, text="Regression")
L14.place(x=10, y=275)
CheckVar5 = IntVar()
CheckVar6 = IntVar()
CheckVar7 = IntVar()
C5 = Checkbutton(top, text="graph", variable=CheckVar5,
                 onvalue=1, offvalue=0)
C6 = Checkbutton(top, text="csv", variable=CheckVar6,
                 onvalue=1, offvalue=0)
C7 = Checkbutton(top, text="accurancy", variable=CheckVar7,
                 onvalue=1, offvalue=0)
C5.place(x=10, y=295)
C6.place(x=80, y=295)
C7.place(x=150, y=295)

L4 = Label(text="Monday")
L4.place(x=300, y=20)
monday = IntVar()
R3 = Checkbutton(top, text="", variable=monday,
                 onvalue=1, offvalue=0)
R3.place(x=275, y=20)

L5 = Label(text="Tuesday")
L5.place(x=300, y=40)
tuesday = IntVar()
R5 = Checkbutton(top, text="", variable=tuesday,
                 onvalue=1, offvalue=0)
R5.place(x=275, y=40)

L6 = Label(text="Wednesday")
L6.place(x=300, y=60)
wednesday = IntVar()
R7 = Checkbutton(top, text="", variable=wednesday,
                 onvalue=1, offvalue=0)
R7.place(x=275, y=60)

L7 = Label(text="Thursday")
L7.place(x=300, y=80)
thursday = IntVar()
R9 = Checkbutton(top, text="", variable=thursday,
                 onvalue=1, offvalue=0)
R9.place(x=275, y=80)

L8 = Label(text="Friday")
L8.place(x=300, y=100)
friday = IntVar()
R11 = Checkbutton(top, text="", variable=friday,
                  onvalue=1, offvalue=0)
R11.place(x=275, y=100)

L9 = Label(text="Saturday")
L9.place(x=300, y=120)
saturday = IntVar()
R13 = Checkbutton(top, text="", variable=saturday,
                  onvalue=1, offvalue=0)
R13.place(x=275, y=120)

L10 = Label(text="Sunday")
L10.place(x=300, y=140)
sunday = IntVar()
R15 = Checkbutton(top, text="", variable=sunday,
                  onvalue=1, offvalue=0)
R15.place(x=275, y=140)

L15 = Label(text="Select Regression Mode:")
L15.place(x=275, y=160)
analysis = IntVar()
R1 = Radiobutton(top, text="Prediction", variable=analysis, value=0)
R1.place(x=275, y=180)
R2 = Radiobutton(top, text="Accuracy", variable=analysis, value=1)
R2.place(x=275, y=200)

button = Button(top, text="Run", command=top.quit)
button.place(x=180, y=320, height=30, width=140)

top.mainloop()
print("HELLO")
print("pdf: " + str(var.get()))
print("Output: " + E1.get())
print("Input: " + E2.get())
print("Important Dates: " + E3.get())
print("Order Graph: " + str(CheckVar1.get()))
print("Order Csv: " + str(CheckVar2.get()))
print("Shift Graph: " + str(CheckVar3.get()))
print("Shift Csv: " + str(CheckVar4.get()))
print("Regression Graph: " + str(CheckVar5.get()))
print("Regression Csv: " + str(CheckVar6.get()))
print("Regression Accurancy: " + str(CheckVar7.get()))
print("Monday: " + str(monday.get()))
print("Tuesday: " + str(tuesday.get()))
print("Wednesday: " + str(wednesday.get()))
print("Thursday: " + str(thursday.get()))
print("Friday: " + str(friday.get()))
print("Saturday: " + str(saturday.get()))
print("Sunday: " + str(sunday.get()))
print("analysis: " + str(analysis.get()))  # File which saves the regression function. 1 = yes, 0 = no

# Fix the first two appends to be dynamic once the date picker is implemented into the GUI.
paramList.append("2015-07-01")  # Start Date
paramList.append("2016-09-01")  # End Date
paramList.append(monday.get())  # Monday flag
paramList.append(tuesday.get())  # Tuesday flag
paramList.append(wednesday.get())  # Wednesday flag
paramList.append(thursday.get())  # Thursday flag
paramList.append(friday.get())  # Friday flag
paramList.append(saturday.get())  # Saturday flag
paramList.append(sunday.get())  # Sunday flag
paramList.append(E1.get())  # output path
paramList.append(E2.get())  # input path
paramList.append(E3.get())  # input path for importantDates.csv
paramList.append(str(CheckVar1.get()))  # order graph
paramList.append(str(CheckVar2.get()))  # order csv
paramList.append(str(CheckVar3.get()))  # shift graph
paramList.append(str(CheckVar4.get()))  # shift csv
paramList.append(str(CheckVar5.get()))  # regression graph
paramList.append(str(CheckVar6.get()))  # regression csv
paramList.append(str(analysis.get()))  # 0 = Prediction Mode, 1 = Accuracy Mode
paramList.append(str(var.get()))  # 0 = ".png", 1 = ".pdf"
