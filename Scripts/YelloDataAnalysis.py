import matplotlib
import csv

matplotlib.use('pdf')
import matplotlib.pyplot as plt
import seaborn.apionly as sns
import pandas as pd
import functools
import datetime as dt
import numpy as np
import sys
import warnings
import os.path

# import tkinter
# top = tkinter.Tk()
# Code to add widgets will go here...
# top.mainloop()

"""
Argument parsing

if a user passes arguments the program assumes they are piping.

[dropdown selection] specify graph filetype (png or pdf)
[7 booleans] datefilter (applies to everything)
[text field]Specify an output folder path
[text field]specify an input file
[text field]Specify important dates csv location (optional [boolean])
Which scans:
ordercount ([boolean] graph) ([boolean] csv) Done
shiftcount ([boolean] graph) ([boolean] csv) Done
regression ([boolean] graph) ([boolean] csv) Done (optional csv that specifies regression summary)
[Boolean] Self Accuracy Analysis
[button] directory valid
[button] file valid
[text field] important dates
[button] run

"""

# comment line 1

# declare all user input

# is User Interface being generated
GenUI = False

# [dropdown selection] specify graph filetype
graphFileType = ''

# [7 booleans] datefilter (applies to everything)
hasMonday = False
hasTuesday = False
hasWednesday = False
hasThursday = False
hasFriday = False
hasSaturday = False
hasSunday = False

# [text field]Specify an output folder path
outputPath = ''

# [text field]specify an input file
inputPath = ''

# [text field]Specify important dates csv location (optional [boolean])
importantDatePath = ''

# ordercount ([boolean] graph) ([boolean] csv)
orderCountGraph = False
orderCountCSV = False

# shiftcount ([boolean] graph) ([boolean] csv)
shiftCountGraph = False
shiftCountCSV = False

# regression ([boolean] graph) ([boolean] csv) (optional csv that specifies regression summary)
regressionGraph = False
regressionCSV = False
# Something else needed???

# [Boolean] Self Accuracy Analysis
selfAccuracyAnalysis = False

# [text field] important dates
importantDates = False

doAccuracy = False

if len(sys.argv) == 23:  # this is because of the date input, if the dates are shifted out of the args we can make this 0 again.
    # initialise all user input variables here if user is piping
    # Expected input format: YelloDataAnalysis(StartDate, EndDate, graphFileType, hasMonday, hasTuesday, hasWednesday, hasThursday, hasFriday, hasSaturday,
    # hasSunday, outputPath, inputPath, importantDatePath, orderCountGraph, orderCountCSV, shiftCountGraph, shiftCountCSV, regressionGraph,
    # regressionCSV, selfAccuracyAnalysis, importantDates)l
    # graphFileType = sys.argv[0]
        if sys.argv[3] == '1':
            hasMonday = True
        elif sys.argv[3] != '0':
            print("unknown input for Monday, defaulting to False")

        if sys.argv[4] == '1':
            hasTuesday = True
        elif sys.argv[4] != '0':
            print("unknown input for Tuesday, defaulting to False")

        if sys.argv[5] == '1':
            hasWednesday = True
        elif sys.argv[5] != '0':
            print("unknown input for Wednesday, defaulting to False")

        if sys.argv[6] == '1':
            hasThursday = True
        elif sys.argv[6] != '0':
            print("unknown input for Thursday, defaulting to False")

        if sys.argv[7] == '1':
            hasFriday = True
        elif sys.argv[7] != '0':
            print("unknown input for Friday, defaulting to False")

        if sys.argv[8] == '1':
            hasSaturday = True
        elif sys.argv[8] != '0':
            print("unknown input for Saturday, defaulting to False")

        if sys.argv[9] == '1':
            hasSunday = True
        elif sys.argv[9] != '0':
            print("unknown input for Sunday, defaulting to False")

        outputPath = sys.argv[10]
        if not os.path.exists(outputPath):
            print(
                "output folder does not exist in the directory given, please create the output directory or point to an existing output directory in the 10th argument")
            sys.exit()

        inputPath = sys.argv[11]
        if not os.path.exists(inputPath + '/Shift_View.csv'):
            print(
                "Shift View does not exist in the directory given, please point to the existing directory containing the file in the 11th argument")
            sys.exit()

        if not os.path.exists(inputPath + '/stores_View.csv'):
            print(
                "stores View does not exist in the directory given, please point to the existing directory containing the file in the 11th argument")
            sys.exit()

        importantDatePath = sys.argv[12]
        if not os.path.exists(inputPath + '/DayEventCalendar.csv'):
            print(
                "Day Event Calendar does not exist in the directory given, please point to the existing directory containing the file in the 12th argument")
            sys.exit()

        if sys.argv[13] == '1':
            orderCountGraph = True
        elif sys.argv[13] != '0':
            print("unknown input for orderCountGraph, defaulting to False")

        if sys.argv[14] == '1':
            orderCountCSV = True
        elif sys.argv[14] != '0':
            print("unknown input for orderCountCSV, defaulting to False")

        if sys.argv[15] == '1':
            shiftCountGraph = True
        elif sys.argv[15] != '0':
            print("unknown input for shiftCountGraph, defaulting to False")

        if sys.argv[16] == '1':
            shiftCountCSV = True
        elif sys.argv[16] != '0':
            print("unknown input for shiftCountCSV, defaulting to False")

        if sys.argv[17] == '1':
            regressionGraph = True
        elif sys.argv[17] != '0':
            print("unknown input for regressionGraph, defaulting to False")

        if sys.argv[18] == '1':
            regressionCSV = True
        elif sys.argv[18] != '0':
            print("unknown input for regressionCSV, defaulting to False")

        if sys.argv[19] == '1':
            selfAccuracyAnalysis = True
        elif sys.argv[19] != '0':
            print("unknown input for selfAccuracyAnalysis, defaulting to False")

        if sys.argv[20] == '1':
            importantDates = True
        elif sys.argv[20] != '0':
            print("unknown input for importantDates, defaulting to False")

        if not (sys.argv[21] == 'pdf' or sys.argv[21] == 'png'):
            print("filetype not a recognised type, please choose png or pdf. [error in arg 21]")
        else:
            graphFileType = sys.argv[21]

        if sys.argv[22] == '1':
            doAccuracy = True

        # print("yer")
else:
    print("yaay")
    # initialise all user input variables here if user is using a ui.
    # genUI

# print("Enter filepath to the shift view csv file eg ../Inputs \n")
csvFolder = inputPath

# print("Enter output folder for weekday data EG '../Inputs'  '../testInputs'  \n")
outputFolder = outputPath

# needs to be implemented

# print("Enter raw_Weather .csv Location ../Inputs/weather_data.csv\n")
# weatherInput = input()

# print("Enter store_locator .csv Location ../Inputs/stores_view.csv\n")
# storeInput = input()

# print("Enter orderCounts .csv Location ../Inputs/orderCounts.csv\n")
# orderCountInput = input()



# Function: Str type -> Date type (YYYY-MM-DD).
to_date = lambda x: dt.datetime.strptime(x, '%Y-%m-%d').date()

# Get shift_views.
raw_shifts = pd.read_csv(csvFolder + '/Shift_View.csv', dtype=None)
raw_shifts['start'] = raw_shifts['start'].str[:-9].apply(to_date)
# Get weather data.
# raw_weather = pd.read_csv(csvFolder + '/weather_data.csv', dtype=None)
# raw_weather['Date'] = raw_weather['Date'].apply(to_date)
# Store locator
store_locator = pd.read_csv(csvFolder + '/stores_view.csv', dtype=None)
holidays = list(map(to_date, ["2016-04-25", "2016-06-12"]))

"""
#(Static) Logical vector: True if row in NSW, False otherwise.
in_nsw = functools.reduce(
         lambda x, y: x & y,
         [raw_shifts['storeId'] != n for n in [118, 187, 191, 195, 196, 198, 203, 205, 206, 209, 210, 221, 222, 240, 242, 246]])
"""
# (Dynamic) Logical vector: True if row in NSW, False otherwise.
# Have filtered out non-Sydney stores (187, 191, 195, 198, 206, 209, 210, 242)
in_nsw = functools.reduce(
    lambda x, y: x & y,
    [raw_shifts['storeId'] != n for n in
     [s for s in
      store_locator[store_locator['administrative_area_level_1'] != 'NSW']['id']]
     ])

# All shifts in NSW.
all_shifts = raw_shifts[in_nsw]

# All valid shifts in NSW. (C == C iff C is not NaN)
valid_shifts = raw_shifts[in_nsw &
                          (raw_shifts['actualStart'] == raw_shifts['actualStart']) &
                          (raw_shifts['actualEnd'] == raw_shifts['actualEnd']) &
                          (raw_shifts['shiftStateId'] == 6)]

if len(sys.argv) > 1:
    valid_shifts = valid_shifts[(valid_shifts['start'] >= to_date(sys.argv[1])) &
                                (valid_shifts['start'] <= to_date(sys.argv[2]))]


# tidy_weather = raw_weather[(raw_weather['Date'] >= valid_shifts['start'].min()) &
#                           (raw_weather['Date'] <= valid_shifts['start'].max())]

# sydney = tidy_weather[tidy_weather['Location'] == 'Sydney']
# sydney_airport = tidy_weather[tidy_weather['Location'] == 'Sydney Airport']


def date_range(df, col):
    """
    Returns incrementing sequence of dates from earliest to latest in df['col'].
    Assert df is a dataframe.
    Assert col is a column name of dates in df.
    """
    s = df[col].min()
    e = df[col].max()
    return [s + dt.timedelta(days=x) for x in range(0, (e - s).days + 1)]


def get_day(d):
    """
    Returns any date as a String in the format DD-MM-YYYY.
    """
    return str(d.day) + '-' + str(d.month) + '-' + str(d.year)


def set_canvas():
    """
    Graphing aesthetics.
    """
    sns.set(font_scale=0.5, style='whitegrid')
    sns.plt.gcf().subplots_adjust(bottom=0.3)
    sns.plt.xticks(rotation=90)


def overlay_weather(dr, station):
    """
    Overlays plane with rainfall, max temp, min temp across dates in dr.
    Assert dr is a list od Date types.
    """
    # Rainfall.
    sns.pointplot(x='Date', y='Rainfall (mm)', order=dr,
                  data=station, color='g', scale=0.3)
    # Max Temp.
    sns.pointplot(x='Date', y='Maximum Temperature (C)', order=dr,
                  data=station, color='r', scale=0.3)
    # Min Temp.
    sns.pointplot(x='Date', y='Minimum Temperature (C)', order=dr,
                  data=station, color='b', scale=0.3)


def plot_shiftcounts(n, s):
    """
    Plots actual shifts per date overlaid on all shifts and saves it to s.
    Assert n is a fresh number.
    Assert s is a file name with pdf extension.
    """
    plt.figure(n)
    set_canvas()
    # Incrementing sequence of dates: [earliest, latest].
    dr = date_range(valid_shifts, 'start')
    # Plot count of all shifts per date.
    ax = sns.countplot(x='start', data=all_shifts, color='r', order=dr)
    # Plot count of valid shifts per date.
    sns.countplot(x='start', data=valid_shifts, color='b', order=dr)
    # Add counts to each bar.
    for i, p in enumerate(ax.patches):
        ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x(), p.get_height() + 0.2))
        if i >= len(dr) and dr[i - len(dr)].weekday() == 4:
            p.set_color('g')

    # Set axes labels.
    ax.set(xlabel='Dates', ylabel='No. of Shifts', title='SHIFTS')
    ax.set_xticklabels([get_day(j) if i % 2 else None for (i, j) in enumerate(dr)])
    # Save.

    if shiftCountGraph:
        sns.plt.savefig(s)

    if shiftCountCSV:
        with open(outputFolder + '/shiftCounts.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(valid_shifts.values)


def organize_data():
    order_counts = valid_shifts.groupby(['start']).sum().reset_index()
    weekdaysList = []
    weeksAndDates = []
    dataFrame = []
    headers = ["mondays", "tuesdays", "wednesdays", "thursdays", "fridays", "saturdays", "sundays"]

    # dataFrame = a dataFrame list to give to the pandas csv_writer.
    # weekdaysList is a list by weekdays. weekdaysList[0] = list of all mondays. weekdaysList[1] = list of all tuesdays and so on...
    # weeksList is a list by weeks. weeksList[0] = list of orders in week 1, weeksList[1] = list of orders in week 2 and so on...

    for i in range(1, 8):
        dataFrame.append(order_counts[order_counts['start'].apply(lambda x: x.isoweekday()) == i])
        weekdaysList.append(list(zip(range(len(dataFrame[-1]['start'])),
                                     dataFrame[-1]['deliveryCount'])))
        weeksAndDates.append(list(zip(dataFrame[-1]['start'],
                                      dataFrame[-1]['deliveryCount'])))

    if doAccuracy:
        comparison = []
        for i in range(len(weekdaysList)):
            comparison.append(weeksAndDates[i][-2:])
            weekdaysList[i] = weekdaysList[i][:-2]
        comparison = sorted([item for sublist in comparison for item in sublist], key=lambda x: x[0])
    else:
        comparison = sorted([item for sublist in weeksAndDates for item in sublist], key=lambda x: x[0])

    # This method later to implement.
    # temp = []
    # temp2 = []
    # for x in inputList:
    #     temp.append(weekdaysList[x - 1])
    #     temp2.append(comparison[x - 1])
    # weekdaysList = temp

    # Traverse a list of weeks and append sublists of size n (n is input()). For example, if n = 3, then:
    # data = [[Week 1, Week 2, Week 3], [Week 2, Week 3, Week4], [Week 3, Week 4, Week5], ... ,[Week x - 2, Week x - 1, Week x]]

    # nDict is a dictionary of size n - init, where each key in nDict (which is a number between n and init)
    # is a dictionary of 'training' dictionaries of iterative size n.

    # "training' is another dictionary, each referencing a weekday in the "headers" list above ^^^
    # Each key in 'training' has a list of lists of size n and has dates and orders partitioned. E.g. if n = 3, then:
    # training["mondays"] = [[Monday of week 1, Monday of week 2, Monday of week 3], [Monday of week 2, Monday of week 3, Monday of week 4],...,[Monday of Week x - b - 2, Monday of Week x - b - 1, Monday of Week x - b]]
    # Where b is the size of the test data

    n = 10
    nDictTraining = {}
    nDictTest = {}
    init = 5
    df = 3
    b = 3

    for x in range(init, n + 1):
        training = {}
        test = {}
        for names in headers:
            training[names] = []
        for weekdayArray, names in zip(weekdaysList, headers):
            for i in range(len(weekdayArray) - x):
                appendable = []
                for j in range(x):
                    appendable.append(weekdayArray[i + j])
                training[names].append(appendable)
        for keys in training:
            test[keys] = training[keys][-b:]

        for keys in training:
            training[keys] = training[keys][:len(training[keys]) - b]

        nDictTraining[x] = training
        nDictTest[x] = test

    # nDictTraining[n]: Get iterations over all weeks partitioned into arrays of size n
    # nDictTraining[n]['monday']: Get all mondays of iterations over weeks of size n
    # nDictTraining[n]['monday'][0]: Get the 1st (AKA the 0th) monday of the mondays of iterations. Paired list [datetime.date, ordercounts]
    # nDictTraining[n]['monday'][0][0]: Get the datetime.date of the 1st (AKA the 0th) monday of iterations

    # print('Train: ', nDictTraining)
    # print('Test:  ', nDictTest)

    # Test accuracy for each dataset
    # d = 3, foir all n
    # Upgrade to use d=1-5
    # build for loop that gets ya to val
    # print(type(nDictTraining))
    # print(len(nDictTraining))

    resultset = {h: [] for h in headers}  # [len(headers)]
    for h in headers:
        for i in range(init, n):  # Say i is 3...
            for d in range(1, df):
                accuracy = []
                for tr in nDictTraining[i][h]:  # For each item in
                    test = np.array(nDictTest[i][h])
                    train = np.array(tr)
                    print(train[:, 0])
                    print(train[:, 1])
                    coefficients = []
                    coefficients = np.polyfit(train[:, 0], train[:, 1], d)
                    print("Coef", coefficients)
                    polynomial = np.poly1d(coefficients)
                    # yfit = np.polyval(coefficients, train[:, 0])

                    print(coefficients)
                    print(polynomial)
                    print(test[:, 0])
                    estimate = []
                    real = []
                    for q in test:
                        for r in q:
                            print("R:", r[0])
                            estimate = np.append(estimate, polynomial(r[0]))
                            real.append(r[1])
                        print(estimate)
                        estimate = np.array(estimate)
                        estimate = np.absolute(estimate)
                        results = estimate - real
                        results = np.absolute(results)
                        results = np.average(results)
                        accuracy.append(results)
                        print("Average error: ", results)
                accuracy = np.average(accuracy)
                # Calculate total accuray
                # train = np.append(train, arr)
                # arrSetLower.append(i,d,polynomial,accuracy)
                # arrSetUpper.append(arrSetLower)
                if not resultset[h] or accuracy < resultset[h][3]:
                    resultset[h] = ([i, d, polynomial, accuracy])

    print("Resultset: ", resultset)
    # for each weekday
    # determine estimated vlu 2 days past range of data
    # create singlar array and pass to matlibplot

    regressions = []

    for x in [1, 2]:
        for day in headers:
            regressions.append(resultset[day][2](x))

    if doAccuracy:
        initDate = comparison[0][0]
        offset = initDate.weekday()
        # What is this doing to 'regressions'?
        regressions = regressions[:7][offset:] + regressions[:7][:offset] + regressions[7:][offset:] + regressions[7:][
                                                                                                       :offset]
        printable, trueCounts = zip(*comparison)

        print()
        print("################### RESULTS BELOW: MODE = ACCURACY ########################")
        print("True Data:")
        print(comparison)
        print()
        print("Predicted Data:")
        print(list(zip(printable, map(int, map(round, regressions)))))

        plt.figure(10)
        plt.plot(printable, regressions, 'r', label="Accuracy")
        plt.plot(printable, trueCounts, 'b', label="True")
        if regressionGraph:
            plt.savefig(outputFolder + "/accuracyGraph." + graphFileType)
        print("End")

        if regressionCSV:
            with open(csvFolder + '/accuracyResults.csv', 'w', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(["Date", "Predicted Value", "True Value"])
                writer.writerows(zip(list(printable), list(regressions), list(trueCounts)))

    else:
        initDate = comparison[-1:][0][0]
        offset = initDate.weekday()

        # What is this doing to 'regressions'?
        regressions = regressions[:7][offset:] + regressions[:7][:offset] + regressions[7:][offset:] + regressions[7:][:offset]
        futureDates = []
        for i in range(14):
            futureDates.append(initDate + dt.timedelta(days=i))

        print()
        print("################### RESULTS BELOW: MODE = PREDICTION ########################")
        print("Predicted Data:")
        print(list(zip(futureDates, map(int, map(round, regressions)))))

        plt.figure(10)
        plt.plot(futureDates, regressions, 'r', label="Prediction")
        if regressionGraph:
            plt.savefig(outputFolder + "/predictionGraph." + graphFileType)
        print("End")

        if regressionCSV:
            with open(csvFolder + '/predictionResults.csv', 'w', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(["Date", "Predicted Value"])
                writer.writerows(zip(list(futureDates), list(regressions)))

    wkdayflag = [hasMonday, hasTuesday, hasWednesday, hasThursday, hasFriday, hasSaturday, hasSunday][::-1]
    for x, days in zip(dataFrame, headers):
        if wkdayflag.pop():
            x[['start', 'deliveryCount']].to_csv(outputFolder + '/' + days + 'OrderCounts.csv')

def plot_ordercounts(n, s):
    """
    Plots count of orders per date and saves it to s.
    Assert n is a fresh number.
    Assert s is a file name with pdf extension.
    """
    # if orderCountGraph
    plt.figure(n)
    set_canvas()
    # Incrementing sequence of dates [earliest, latest].
    dr = date_range(valid_shifts, 'start')
    # Aggregate all orders by date.
    # (Index must be reset as 'start' is auto-set to index and no longer a column.)
    order_counts = valid_shifts.groupby(['start']).sum().reset_index()

    if orderCountGraph:

        # Plot count of orders per date.
        ax = sns.barplot(x='start', y='deliveryCount',
                         data=order_counts, color='w', order=dr)

        # Add counts to each bar.
        for p in ax.patches:
            ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x(), p.get_height() + 0.2))

        # Weather.
        # overlay_weather(dr, station)
        # Set axes labels.
        ax.set(xlabel='Dates', ylabel='No. of Orders', title='ORDERS')
        ax.set_xticklabels([get_day(j) if i % 2 else ' ' for (i, j) in enumerate(dr)])
        # Limit y-axis range to non-negative values.
        # (Rainfall levels are near enough to 0 to auto-set lower bound to -20.)
        ax.axes.set_ylim(0, )
        # Save.

        sns.plt.savefig(s)

    if orderCountCSV:
        with open(outputFolder + '/orderCounts.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(["start", "deliveryCount"])
            writer.writerows(list(zip(order_counts['start'], order_counts['deliveryCount'])))


if __name__ == "__main__":
    # print("Please input which days of the week you want. 1 = Monday, 2 = Tuesdays... Separate with space ' '")

    plot_shiftcounts(1, outputFolder + '/shift_counts.' + graphFileType)
    plot_ordercounts(2, outputFolder + '/order_counts_obs.' + graphFileType)
    # plot_ordercounts(3, '../images/order_counts_airport.pdf', sydney_airport)

    print("Type a for accuracy, p for prediction")
    organize_data()
    # [int(x) for x in input().split()]


    # import unittest
    #
    # class myTest(unittest.TestCase):
    #     def test_main(self):
    #         poop = dt.date(2016, 10, 10);
    #
    #
    #         self.assertEqual( get_day(poop), '10-10-2016' )
    #
    #     def test_instantiation(self):
    #
    #         self.assertIsNotNone(raw_shifts)
    #         self.assertIsNotNone(raw_weather)
    #         self.assertIsNotNone(store_locator)
    #
    #     #def test_overArching(self):
    #         #make a test that creates a cheksum based on the expected output and then calculate a checksum based on actual output. if these two are the same it should be ok???
    #
    # unittest.main()
