import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
import seaborn.apionly as sns
import numpy as np
import pandas as pd
import math as mt

import datetime as dt

import functools
import csv
import sys
import warnings
import os.path

# Function: Str type -> Date type (YYYY-MM-DD).
str_to_date = lambda x: dt.datetime.strptime(x, '%Y-%m-%d').date()

def date_range(df, col):
    """ 
    Returns incrementing sequence of dates from earliest to latest in df['col'].
    Assert df is a dataframe.
    Assert col is a column name of dates in df.
    """
    s = df[col].min()
    e = df[col].max()
    return [s + dt.timedelta(days=x) for x in range(0, (e - s).days + 1)] 

def get_day(d):
    """ 
    Returns any date as a String in the format DD-MM-YYYY.
    """
    return str(d.day) + '-' + str(d.month) + '-' + str(d.year)
