from helper import *
import urllib.request

def update_weather():
    base_url = "http://www.bom.gov.au/climate/dwo/{0}/text/IDCJDW{1}.{0}.csv"
    #eg:        www.bom.gov.au/climate/dwo/201507/text/IDCJDW2001.201507.csv
    #range of {0} : 2001 - 2153 for NSW

    t = dt.datetime.now()
    cur_year = t.year
    cur_month = t.month
    dates = [str(u)+str(v).zfill(2) 
             for u in range(2015,cur_year+1) 
             for v in range(1,13) 
                if (u != 2015 or v > 8)
                and
                (u != cur_year or v <= cur_month)]
    #earliest date in the BOM's historical data is 2015-07
    #latest sensible date is the current month of the current year

    header = ["Location",
              "Date",
              "Minimum Temperature (C)",
              "Maximum Temperature (C)",
              "Rainfall (mm)",
              "Evaporation (mm)",
              "Sunshine (hours)",
              "Direction of Maximum Wind Gust",
              "Speed of Maximum Wind Gust (km/h)",
              "Time of Maximum Wind Gust",
              "9am Temperature (C)",
              "9am Relative Humidity (%)",
              "9am Cloud Amount (oktas)",
              "9am Wind Direction",
              "9am Wind Speed (km/h)",
              "9am MSL Pressure (hPa)",
              "3pm Temperature (C)",
              "3pm Relative Humidity (%)",
              "3pm Cloud Amount (oktas)",
              "3pm Wind Direction",
              "3pm Wind Speed (km/h)",
              "3pm MSL Pressure (hPa)"]

    with open("../Inputs/weather_data.csv", "w") as weather_data:
        out = csv.writer(weather_data)
        out.writerow(header)

        #sydney, observatory hill code is 2124
        #sydney, airport code is 2125
        for station in [2124, 2125]:
            for d in dates:
                f = urllib.request.urlopen(base_url.format(d, station))
                loc = f.readline().decode('utf-8')
                #name of location begins at index 32 and ends at the first ','
                loc = [loc[32:loc.index(',')],]
                #lines that begin with x st ord(x) == 34 are irrelevant
                while f.readline()[0] == 34:
                    pass
                #eat blank line
                f.readline()
                #get lines of interest
                for line in f:
                    data = line.decode('utf-8').strip('\n').strip('\r')[1:].split(',')
                    out.writerow(loc + data)
                f.close()

    weather_data.close()
