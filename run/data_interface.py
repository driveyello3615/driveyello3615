from helper import *

rs = None
rw = None
allshifts = None
validshifts = None
sl = None
tw = None
id = None
# nswOnly = False

#
# def setNSWOnlyTrue():
#     global nswOnly
#     nswOnly = True
#
#
# def setNSWOnlyFalse():
#     global nswOnly
#     nswOnly = False


def load_important_dates(f):
    global id
    if id is None:
        id = pd.read_csv(f, dtype=None, usecols=[0, 1])
    return id


# def store_locator(f):
#     global sl
#     if sl is None:
#         sl = pd.read_csv('../Inputs/stores_view.csv', dtype=None)
#     return sl


def raw_shifts(f):
    global rs
    if rs is None:
        rs = pd.read_csv(f, dtype=None)
        # Filters out days when dates are NaN. NaN == NaN always evaluates to False.
        rs = rs[rs['actualStart'] == rs['actualStart']]
        rs['actualStart'] = rs['actualStart'].str[:-9].apply(str_to_date)
    return rs


"""
BEGIN Helper
Logical vector: True if row in NSW, False otherwise.
Have filtered out non-Sydney stores (187, 191, 195, 198, 206, 209, 210, 242)
"""
in_nsw = lambda f: functools.reduce(
    lambda x, y: x & y,
    [raw_shifts(f)['storeId'] != n for n in
     [s for s in
      store_locator(f)[store_locator(f)['administrative_area_level_1'] != 'NSW']['id']]])

"""
END Helper
"""


# def raw_weather(f):
#     global rw
#     if rw is None:
#         rw = pd.read_csv('../Inputs/weather_data.csv', dtype=None)
#         rw['Date'] = rw['Date'].apply(str_to_date)
#     return rw

def all_shifts(f):
    global allshifts
    if allshifts is None:
        # if nswOnly:
        #     allshifts = raw_shifts(f)[in_nsw(f)]
        # else:
            allshifts = raw_shifts(f)
    return allshifts


def valid_shifts(f):
    global validshifts
    if validshifts is None:
        # if nswOnly:
        #     validshifts = raw_shifts(f)[
        #         (in_nsw(f)) &
        #         (raw_shifts(f)['actualStart'] == raw_shifts(f)['actualStart']) &
        #         (raw_shifts(f)['actualEnd'] == raw_shifts(f)['actualEnd']) &
        #         (raw_shifts(f)['shiftStateId'] == 6)]
        # else:
            validshifts = raw_shifts(f)[
                (raw_shifts(f)['actualStart'] == raw_shifts(f)['actualStart']) &
                (raw_shifts(f)['actualEnd'] == raw_shifts(f)['actualEnd']) &
                (raw_shifts(f)['shiftStateId'] == 6)]

    return validshifts


#
# def tidy_weather(f):
#     global tw
#     if tw is None:
#         tw = raw_weather(f)[(raw_weather(f)['Date'] >= valid_shifts(f)['actualStart'].min()) &
#                             (raw_weather(f)['Date'] <= valid_shifts(f)['actualStart'].max())]
#     return tw
#
# sydney = lambda x: tidy_weather(x)[tidy_weather(x)['Location'] == 'Sydney']
# sydney_airport = lambda x: tidy_weather(x)[tidy_weather(x)['Location'] == 'Sydney Airport']

def reset_cache():
    global rs, rw, allshifts, validshifts, sl, tw, id
    rs = None
    rw = None
    allshifts = None
    validshifts = None
    sl = None
    tw = None
    id = None
