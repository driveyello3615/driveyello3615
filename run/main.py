from helper import *
import data_interface as di
# !/usr/bin/python
from tkinter import *
import time

"""
Argument parsing

if a user passes arguments the program assumes they are piping

[dropdown selection] specify graph filetype (png or pdf)
[7 booleans] datefilter (applies to everything)
[text field]Specify an output folder path
[text field]specify an input file
[text field]Specify important dates csv location (ope (optional csv that specifies regression summary)
[Boolean] Self Accuracy Analysistional [boolean])
Which scans:
ordercount ([boolean] graph) ([boolean] csv) Done
shiftcount ([boolean] graph) ([boolean] csv) Done
regression ([boolean] graph) ([boolean] csv) Done
[button] directory valid
[button] file valid
[text field] important dates
[button] run

"""

# declare all user input
date1 = ''
date2 = ''

# is User Interface being generated
GenUI = False

# [dropdown selection] specify graph filetype
graphFileType = ''

# [7 booleans] datefilter (applies to everything)
hasMonday = False
hasTuesday = False
hasWednesday = False
hasThursday = False
hasFriday = False
hasSaturday = False
hasSunday = False

# [text field]Specify an output folder path
outputPath = ''

# [text field]Specify important dates csv location (optional [boolean])
importantDatePath = ''

# ordercount ([boolean] graph) ([boolean] csv)
orderCountGraph = False
orderCountCSV = False

# shiftcount ([boolean] graph) ([boolean] csv)
shiftCountGraph = False
shiftCountCSV = False

# regression ([boolean] graph) ([boolean] csv) (optional csv that specifies regression summary)
regressionGraph = False
regressionCSV = False
# Something else needed???

# [Boolean] Self Accuracy Analysis
selfAccuracyAnalysis = False

# Regression Function output
regressionOut = False

# Advanced settings
importantDateFlag = False
geoList = None
maxN = 0
maxdf = 0

# Generation date
# Generates a string like 20161026-184553 = Oct 26 2016 18:45:53
created = timestr = time.strftime("%Y%m%d-%H%M%S")

def executeUI():
    top = Tk()
    top.resizable(width=False, height=False)
    top.minsize(600, 450)
    top.wm_title("Drive Yello Anaylsis")

    graphTypeLabel = Label(text="Specify graph file type:")
    graphTypeLabel.place(x=0, y=0)
    graphType = IntVar()
    graphTypeButtonPNG = Radiobutton(top, text="png", variable=graphType, value=0)
    graphTypeButtonPNG.place(x=0, y=20)
    graphTypeButtonPDF = Radiobutton(top, text="pdf", variable=graphType, value=1)
    graphTypeButtonPDF.place(x=60, y=20)

    outputLabel = Label(top, text="Specify file output")
    outputLabel.place(x=10, y=40)
    outputEntry = Entry(top, bd=5)
    outputEntry.insert(END, '../output')
    outputEntry.place(x=10, y=60)

    inputLabel = Label(top, text="Specify input file")
    inputLabel.place(x=10, y=85)
    inputEntry = Entry(top, bd=5)
    inputEntry.insert(END, '../Inputs/shift_view_october.csv')
    inputEntry.place(x=10, y=105)

    datesLabel = Label(top, text="Specify important dates csv location (optional)")
    datesLabel.place(x=10, y=175)
    datesEntry = Entry(top, bd=5)
    datesEntry.insert(END, '../Inputs/DayEventCalendar.csv')
    datesEntry.place(x=10, y=195)

    generalOutputsLabel = Label(top, text="Outputs:")
    generalOutputsLabel.place(x=10, y=220)
    orderCountLabel = Label(top, text="Order Count:")
    orderCountLabel.place(x=10, y=240)
    orderCountGraphValue = IntVar()
    orderCountCsvValue = IntVar()
    orderCountGraphButton = Checkbutton(top, text="Graph", variable=orderCountGraphValue,
                                        onvalue=1, offvalue=0)
    orderCountCsvButton = Checkbutton(top, text="Csv", variable=orderCountCsvValue,
                                      onvalue=1, offvalue=0)
    orderCountGraphButton.place(x=10, y=260)
    orderCountCsvButton.place(x=80, y=260)
    shiftCountLabel = Label(top, text="Shift Count:")
    shiftCountLabel.place(x=10, y=280)
    shiftCountGraphValue = IntVar()
    shiftCountCsvValue = IntVar()
    shiftCountGraphButton = Checkbutton(top, text="Graph", variable=shiftCountGraphValue,
                                        onvalue=1, offvalue=0)
    shiftCountCsvButton = Checkbutton(top, text="Csv", variable=shiftCountCsvValue,
                                      onvalue=1, offvalue=0)
    shiftCountGraphButton.place(x=10, y=300)
    shiftCountCsvButton.place(x=80, y=300)
    regressionLabel = Label(top, text="Regression:")
    regressionLabel.place(x=10, y=320)
    regressionGraph = IntVar()
    regressionCsv = IntVar()
    regressionAccuracy = IntVar()
    regressionGraphButton = Checkbutton(top, text="Graph", variable=regressionGraph,
                                        onvalue=1, offvalue=0)
    regressionCsvButton = Checkbutton(top, text="Csv", variable=regressionCsv,
                                      onvalue=1, offvalue=0)
    regressionAccuracyButton = Checkbutton(top, text="Reg. Functions", variable=regressionAccuracy)

    regressionGraphButton.place(x=10, y=340)
    regressionCsvButton.place(x=80, y=340)
    regressionAccuracyButton.place(x=150, y=340)

    mondayLabel = Label(text="Monday")
    mondayLabel.place(x=345, y=20)
    monday = IntVar()
    mondayButton = Checkbutton(top, text="", variable=monday,
                               onvalue=0, offvalue=1)
    mondayButton.place(x=320, y=20)

    tuesdayLabel = Label(text="Tuesday")
    tuesdayLabel.place(x=345, y=40)
    tuesday = IntVar()
    tuesdayButton = Checkbutton(top, text="", variable=tuesday,
                                onvalue=0, offvalue=1)
    tuesdayButton.place(x=320, y=40)

    wednesdayLabel = Label(text="Wednesday")
    wednesdayLabel.place(x=345, y=60)
    wednesday = IntVar()
    wednesdayButton = Checkbutton(top, text="", variable=wednesday,
                                  onvalue=0, offvalue=1)
    wednesdayButton.place(x=320, y=60)

    thursdayLabel = Label(text="Thursday")
    thursdayLabel.place(x=345, y=80)
    thursday = IntVar()
    thursdayButton = Checkbutton(top, text="", variable=thursday,
                                 onvalue=0, offvalue=1)
    thursdayButton.place(x=320, y=80)

    fridayLabel = Label(text="Friday")
    fridayLabel.place(x=345, y=100)
    friday = IntVar()
    fridayButton = Checkbutton(top, text="", variable=friday,
                               onvalue=0, offvalue=1)
    fridayButton.place(x=320, y=100)

    saturdayLabel = Label(text="Saturday")
    saturdayLabel.place(x=345, y=120)
    saturday = IntVar()
    saturdayButton = Checkbutton(top, text="", variable=saturday,
                                 onvalue=0, offvalue=1)
    saturdayButton.place(x=320, y=120)

    sundayLabel = Label(text="Sunday")
    sundayLabel.place(x=345, y=140)
    sunday = IntVar()
    sundayButton = Checkbutton(top, text="", variable=sunday,
                               onvalue=0, offvalue=1)
    sundayButton.place(x=320, y=140)

    regressionModeLabel = Label(text="Select Regression Mode:")
    regressionModeLabel.place(x=320, y=160)
    analysis = IntVar()
    regressionModePredictionButton = Radiobutton(top, text="Prediction", variable=analysis, value=0)
    regressionModePredictionButton.place(x=320, y=180)
    regressionModeAccuracyButton = Radiobutton(top, text="Accuracy", variable=analysis, value=1)
    regressionModeAccuracyButton.place(x=320, y=200)

    advancedLabel = Label(top, text="--Advanced Settings:--")
    advancedLabel.place(x=320, y=240)

    filterOutlierFlag = IntVar()
    outlierCheck = Checkbutton(top, text="Filter out special event days", variable=filterOutlierFlag, onvalue=1,
                               offvalue=0)
    outlierCheck.place(x=320, y=260)

    dfEntryLabel = Label(top, text="Specify maxiumum degree of freedom (df > 0)")
    dfEntryLabel.place(x=320, y=290)
    dfEntry = Entry(top, bd=5)
    dfEntry.place(x=320, y=310)

    nEntryLabel = Label(top, text="Specify maxiumum 'n' value (n > df+1)")
    nEntryLabel.place(x=320, y=330)
    nEntry = Entry(top, bd=5)
    nEntry.place(x=320, y=350)

    storeLabel = Label(text="Store Numbers")
    storeLabel.place(x=10, y=130)
    storeTextBox = Entry(top, bd=5)
    storeTextBox.place(x=10, y=150)

    button = Button(top, text="Run", command=top.quit)
    button.place(x=15, y=375, height=60, width=140)

    top.mainloop()

    # Fix the first two appends to be dynamic once the date picker is implemented into the GUI.
    paramList = []
    # Resets the cache to prevent crashing when adding in new data with dates that go out of the range of the previous cache.
    di.reset_cache()  # Load parameters

    paramList.append(monday.get())  # Monday flag
    paramList.append(tuesday.get())  # Tuesday flag
    paramList.append(wednesday.get())  # Wednesday flag
    paramList.append(thursday.get())  # Thursday flag
    paramList.append(friday.get())  # Friday flag
    paramList.append(saturday.get())  # Saturday flag
    paramList.append(sunday.get())  # Sunday flag
    paramList.append(outputEntry.get())  # output path
    paramList.append(inputEntry.get())  # input path
    paramList.append(datesEntry.get())  # input path for importantDates.csv
    paramList.append(int(orderCountGraphValue.get()))  # order graph
    paramList.append(int(orderCountCsvValue.get()))  # order csv
    paramList.append(int(shiftCountGraphValue.get()))  # shift graph
    paramList.append(int(shiftCountCsvValue.get()))  # shift csv
    paramList.append(int(regressionGraph.get()))  # regression graph
    paramList.append(int(regressionCsv.get()))  # regression csv
    paramList.append(int(analysis.get()))  # 0 = Prediction Mode, 1 = Accuracy Mode
    paramList.append(int(graphType.get()))  # 0 = ".png", 1 = ".pdf"
    paramList.append(str(storeTextBox.get()))
    paramList.append(filterOutlierFlag.get())
    paramList.append(dfEntry.get())
    paramList.append(nEntry.get())
    paramList.append(regressionAccuracy.get())
    return paramList

if len(sys.argv) > 1:
    if len(sys.argv) == 24:
        # this is because of the date input, if the dates are shifted out of the args we can make this 0 again.
        # initialise all user input variables here if user is piping.

        if sys.argv[1] == '1':
            hasMonday = True
        elif sys.argv[1] != '0':
            print("unknown input for Monday, defaulting to False")

        if sys.argv[2] == '1':
            hasTuesday = True
        elif sys.argv[2] != '0':
            print("unknown input for Tuesday, defaulting to False")

        if sys.argv[3] == '1':
            hasWednesday = True
        elif sys.argv[3] != '0':
            print("unknown input for Wednesday, defaulting to False")

        if sys.argv[4] == '1':
            hasThursday = True
        elif sys.argv[4] != '0':
            print("unknown input for Thursday, defaulting to False")

        if sys.argv[5] == '1':
            hasFriday = True
        elif sys.argv[5] != '0':
            print("unknown input for Friday, defaulting to False")

        if sys.argv[6] == '1':
            hasSaturday = True
        elif sys.argv[6] != '0':
            print("unknown input for Saturday, defaulting to False")

        if sys.argv[7] == '1':
            hasSunday = True
        elif sys.argv[7] != '0':
            print("unknown input for Sunday, defaulting to False")

        outputPath = sys.argv[8]
        if not os.path.exists(outputPath):
            print(
                "output folder does not exist in the directory given, please create the output directory or point to an existing output directory in the 10th argument")
            sys.exit()

        shiftViewDir = sys.argv[9]
        if not os.path.exists(shiftViewDir):
            print(
                "Shift View does not exist in the directory given, please point to the existing directory containing the file in the 11th argument")
            sys.exit()

        importantDatePath = sys.argv[10]
        if sys.argv[10] is not '':
            if not os.path.exists(importantDatePath):
                print(
                    "Day Event Calendar does not exist in the directory given, please point to the existing directory containing the file in the 12th argument")
                sys.exit()
            importantDateFlag = True

        if sys.argv[11] == '1':
            orderCountGraph = True
        elif sys.argv[11] != '0':
            print("unknown input for orderCountGraph, defaulting to False")

        if sys.argv[12] == '1':
            orderCountCSV = True
        elif sys.argv[12] != '0':
            print("unknown input for orderCountCSV, defaulting to False")

        if sys.argv[13] == '1':
            shiftCountGraph = True
        elif sys.argv[13] != '0':
            print("unknown input for shiftCountGraph, defaulting to False")

        if sys.argv[14] == '1':
            shiftCountCSV = True
        elif sys.argv[14] != '0':
            print("unknown input for shiftCountCSV, defaulting to False")

        if sys.argv[15] == '1':
            regressionGraph = True
        elif sys.argv[15] != '0':
            print("unknown input for regressionGraph, defaulting to False")

        if sys.argv[16] == '1':
            regressionCSV = True
        elif sys.argv[16] != '0':
            print("unknown input for regressionCSV, defaulting to False")

        if sys.argv[17] == '1':
            selfAccuracyAnalysis = True
        elif sys.argv[17] != '0':
            print("unknown input for selfAccuracyAnalysis, defaulting to False")

        if sys.argv[18] == '1':
            graphFileType = 'pdf'
        elif sys.argv[18] == '0':
            graphFileType = 'png'
        else:
            print("unknown input for importantDates, defaulting to False")

        if sys.argv[19] != '0':
            geoList = list(map(int, sys.argv[19].split(',')))
        else:
            geoList = None

        if sys.argv[20] == '1':
            importantDateFlag = True

        if sys.argv[21] != '0' and int(sys.argv[21]) > 0:
            maxdf = int(sys.argv[21])
        else:
            maxdf = 0
            print("Invalid degree of freedom, defaulting to <3>")

        if sys.argv[22] != '0' and int(sys.argv[22]) > maxdf + 1:
            maxN = int(sys.argv[22])
        else:
            maxN = 0
            print("Invalid 'n', defaulting to <no. weeks in dataset>")

        if sys.argv[23] == '1':
            regressionOut = True
        elif sys.argv[23] != '0':
            print("Defaulting to NOT printing regression model")

    else:
        print("Argument count is incorrect")
        sys.exit()

else:
    print("Executing UI...")
    paramList = executeUI()

    if paramList[0] == 0:
        hasMonday = True

    if paramList[1] == 0:
        hasTuesday = True

    if paramList[2] == 0:
        hasWednesday = True

    if paramList[3] == 0:
        hasThursday = True

    if paramList[4] == 0:
        hasFriday = True

    if paramList[5] == 0:
        hasSaturday = True

    if paramList[6] == 0:
        hasSunday = True

    outputPath = paramList[7]
    if not os.path.exists(outputPath):
        print(
            "output folder does not exist in the directory given, please create the output directory or point to an existing output directory in the 10th argument")
        sys.exit()

    shiftViewDir = paramList[8]
    if not os.path.exists(shiftViewDir):
        print(
            "Shift View does not exist in the directory given, please point to the existing directory containing the file in the 11th argument")
        sys.exit()

    importantDatePath = paramList[9]
    if paramList[9] is not '':
        if not os.path.exists(importantDatePath):
            print(
                "Day Event Calendar does not exist in the directory given, please point to the existing directory containing the file in the 12th argument")
            sys.exit()

    if paramList[10] == 1:
        orderCountGraph = True

    if paramList[11] == 1:
        orderCountCSV = True

    if paramList[12] == 1:
        shiftCountGraph = True

    if paramList[13] == 1:
        shiftCountCSV = True

    if paramList[14] == 1:
        regressionGraph = True

    if paramList[15] == 1:
        regressionCSV = True

    if paramList[16] == 1:
        selfAccuracyAnalysis = True

    if paramList[17] == 1:
        graphFileType = 'pdf'
    else:
        graphFileType = 'png'

    if paramList[18] != '':
        geoList = list(map(int, paramList[18].split(',')))
    else:
        geoList = None

    if paramList[19] == 1:
        importantDateFlag = True

    if str(paramList[20]) != '' and int(paramList[20]) > 0:
        maxdf = int(paramList[20])
    else:
        maxdf = 0
        print("Defaulting 'df' to <3>")

    if str(paramList[21]) != '' and int(paramList[21]) > maxdf + 1:
        maxN = int(paramList[21])
    else:
        maxN = 0
        print("Defaulting 'n' to <no. weeks in dataset>")

    if paramList[22] == 1:
        regressionOut = True

# Cached all_shifts.
all_shifts_di = di.all_shifts(shiftViewDir)
# Cached valid shifts.
valid_shifts_di = di.valid_shifts(shiftViewDir)

def set_canvas():
    """
    Graphing aesthetics.
    """
    sns.set(font_scale=0.5, style='whitegrid')
    sns.plt.gcf().subplots_adjust(bottom=0.3)
    sns.plt.xticks(rotation=90)


# def overlay_weather(dr, station):
#     """
#     Overlays plane with rainfall, max temp, min temp across dates in dr.
#     Assert dr is a list od Date types.
#     """
#     # Rainfall.
#     sns.pointplot(x='Date', y='Rainfall (mm)', order=dr,
#                   data=station, color='g', scale=0.3)
#     # Max Temp.
#     sns.pointplot(x='Date', y='Maximum Temperature (C)', order=dr,
#                   data=station, color='r', scale=0.3)
#     # Min Temp.
#     sns.pointplot(x='Date', y='Minimum Temperature (C)', order=dr,
#                   data=station, color='b', scale=0.3)


def plot_shiftcounts(n, s):
    """
    Plots actual shifts per date overlaid on all shifts and saves it to s.
    Assert n is a fresh number.
    Assert s is a file name with pdf extension.
    """

    plt.figure(n)
    set_canvas()
    # Incrementing sequence of dates: [earliest, latest].
    dr = date_range(valid_shifts_di, 'actualStart')
    # Plot count of all shifts per date.
    ax = sns.countplot(x='actualStart', data=all_shifts_di, color='r', order=dr)
    # Plot count of valid shifts per date.
    sns.countplot(x='actualStart', data=valid_shifts_di, color='b', order=dr)
    # Add counts to each bar.
    for i, p in enumerate(ax.patches):
        ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x(), p.get_height() + 0.2))
        if i >= len(dr) and dr[i - len(dr)].weekday() == 4:
            p.set_color('g')

    # Set axes labels.
    ax.set(xlabel='Dates', ylabel='No. of Shifts', title='SHIFTS')
    ax.set_xticklabels([get_day(j) if i % 2 else None for (i, j) in enumerate(dr)])
    # Save.

    if shiftCountGraph:
        # was sns.plt.savefig(s)
        sns.plt.savefig('../output/shiftCountGraph%s.png' % created)

    if shiftCountCSV:
        with open(outputPath + '/shiftCounts' + created + '.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerows(valid_shifts_di.values)
    return sns

def plot_ordercounts(n, s, testarg=None):
    """
    Plots count of orders per date and saves it to s.
    Assert n is a fresh number.
    Assert s is a file name with pdf extension.
    """

    # if orderCountGraph
    plt.figure(n)
    set_canvas()
    # Incrementing sequence of dates [earliest, latest].
    dr = date_range(valid_shifts_di, 'actualStart')
    # Aggregate all orders by date.
    # (Index must be reset as 'actualStart' is auto-set to index and no longer a column.)
    order_counts = valid_shifts_di.groupby(['actualStart']).sum().reset_index()

    if orderCountGraph:
        # Plot count of orders per date.
        ax = sns.barplot(x='actualStart', y='deliveryCount',
                         data=order_counts, color='w', order=dr)

        # Add counts to each bar.
        for p in ax.patches:
            ax.annotate('{:.0f}'.format(p.get_height()), (p.get_x(), p.get_height() + 0.2))

        # Weather.
        # overlay_weather(dr, station)
        # Set axes labels.
        ax.set(xlabel='Dates', ylabel='No. of Orders', title='ORDERS')
        ax.set_xticklabels([get_day(j) if i % 2 else ' ' for (i, j) in enumerate(dr)])
        # Limit y-axis range to non-negative values.
        # (Rainfall levels are near enough to 0 to auto-set lower bound to -20.)
        ax.axes.set_ylim(0, )
        # Save.

        sns.plt.savefig(s)

    if orderCountCSV:
        with open(outputPath + '/orderCounts' + created + '.csv', 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(["start", "deliveryCount"])
            writer.writerows(list(zip(order_counts['actualStart'], order_counts['deliveryCount'])))

    return order_counts

def filter_order_counts():
    # This method returns filtered data for orders that do not have a storeID in 'geoList'.
    # If there are no stores with any valid storeIDs, then the program will quit itself.
    order_counts = valid_shifts_di.groupby(['actualStart', 'storeId']).sum().reset_index()

    if os.path.exists(outputPath + "/storeOrders.csv"):
        with open(outputPath + "/storeOrders.csv", 'w', newline='') as f:
            writer = csv.writer(f)
            stores = list(order_counts.groupby(['storeId']).sum().reset_index()['storeId'])
            orders = list(order_counts.groupby(['storeId']).sum().reset_index()['deliveryCount'])
            writer.writerow(['storeId', 'orderCount'])
            writer.writerows(zip(stores, orders))

    if geoList is not None:
        order_counts = order_counts.loc[order_counts['storeId'].isin(geoList)].groupby(
            ['actualStart']).sum().reset_index()
    else:
        order_counts = order_counts.groupby(['actualStart']).sum().reset_index()

    if order_counts.empty:
        print("There are no stores with the input storeIDs")
        sys.exit()
    return order_counts

def regression(n,df,nDictTraining,nDictTest,init,headers):
    resultset = {h: [] for h in headers}  # [len(headers)]
    for h in headers:
        for i in range(init, n):
            for d in range(1, df):
                accuracy = []
                for tr in nDictTraining[i][h]:
                    test = np.array(nDictTest[i][h])
                    train = np.array(tr)
                    coefficients = np.polyfit(train[:, 0], train[:, 1], d)
                    polynomial = np.poly1d(coefficients)
                    estimate = []
                    real = []
                    for q in test:
                        for r in q:
                            estimate = np.append(estimate, polynomial(r[0]))
                            real.append(r[1])
                        estimate = np.array(estimate)
                        estimate = np.absolute(estimate)
                        results = estimate - real
                        results = np.absolute(results)
                        results = np.average(results)
                        accuracy.append(results)
                accuracy = np.average(accuracy)
                if not resultset[h] or accuracy < resultset[h][3]:
                    resultset[h] = ([i, d, polynomial, accuracy])

    return resultset


def perform_analysis(testarg=None):
    order_counts = filter_order_counts()
    if testarg is not None:
        order_counts = pd.read_csv(testarg)
        order_counts['actualStart'] = order_counts['actualStart'].apply(str_to_date)
    enumOrdersList = []
    ordersList = []
    dataFrame = []
    trueData = []
    headers = ["mondays",
               "tuesdays",
               "wednesdays",
               "thursdays",
               "fridays",
               "saturdays",
               "sundays"]

    if importantDateFlag:
        importantOrders = []
        str_to_date_r = lambda x: dt.datetime.strptime(x, '%d/%m/%Y').date()
        loadDi = di.load_important_dates(importantDatePath)
        importantDates = list(loadDi['Date'].apply(str_to_date_r))
        importantEvents = dict(zip(list(loadDi['Date'].apply(str_to_date_r)), list(loadDi['Event'])))

    # dataFrame = a dataFrame list to give to the pandas csv_writer.
    # ordersList is a list of all dates and their orders. Organized into weekdays. Does not include important days.
    # enumeratedList is the same as ordersList but has enumeration instead of dates
    # importantOrders is a list of all dates and their orderCounts that occur on important days.

    for i in range(1, 8):
        dataFrame.append(order_counts[order_counts['actualStart'].apply(lambda x: x.isoweekday()) == i])
        ordersList.append(list(zip(dataFrame[-1]['actualStart'],
                                   dataFrame[-1]['deliveryCount'])))
        if importantDateFlag:
            importantOrders.append([x for x in ordersList[-1] if x[0] in importantDates and x is not None])
            ordersList[-1] = [x for x in ordersList[-1] if x[0] not in importantDates]
        enumOrdersList.append(list(enumerate([x[1] for x in ordersList[-1]])))

    if importantDateFlag:
        importantData = []
        importantOrders = [item for sublist in importantOrders for item in sublist]
        for data in importantOrders:
            if len(data) > 0:
                if data[0] in importantEvents.keys():
                    importantData.append([data[0], data[1], importantEvents[data[0]]])

    if selfAccuracyAnalysis:
        for i in range(len(enumOrdersList)):
            trueData.append(ordersList[i][-2:])
            enumOrdersList[i] = enumOrdersList[i][:-2]
        trueData = sorted([item for sublist in trueData for item in sublist],
                          key=lambda x: x[0])
    else:
        trueData = sorted([item for sublist in ordersList for item in sublist],
                          key=lambda x: x[0])

    # Group weekdays into n sized lists and iterating through every week. For example, if n = 3, then:
    # nDictTraining[3] = [[Week 1, Week 2, Week 3], [Week 2, Week 3, Week4], [Week 3, Week 4, Week5], ... ,[Week x - 2, Week x - 1, Week x]]

    # nDictTraining is a dictionary of size n - init, where each key in nDictTraining (which is a number between n and init)
    # is a dictionary with keys in the 'header' list.
    # Each key in 'nDictTraining' has a list of lists of size n and has dates and orders partitioned. E.g. if n = 3, then:
    # nDictTraining[3]['mondays'] = [[Monday of week 1, Monday of week 2, Monday of week 3], [Monday of week 2, Monday of week 3, Monday of week 4],...,[Monday of Week x - b - 2, Monday of Week x - b - 1, Monday of Week x - b]]
    # Where b is the size of the test data.
    # nDicTest is similar to nDictTraining. It is true data which is shaved off so we can determine the error of our prediction.
    # By using this we can find the best regression model with the least error which will be stored into a list called 'resultSet'


    noWeeks = []
    if maxN == 0:
        for sizes in ordersList:
            noWeeks.append(len(sizes))
        n = min(noWeeks)
    else:
        n = maxN + 1

    if maxdf == 0:
        df = 4
    else:
        df = maxdf + 1

    nDictTraining = {}
    nDictTest = {}
    init = df + 1
    b = 3

    # nDictTraining[n]: Get iterations over all weeks partitioned into arrays of size n
    # nDictTraining[n]['monday']: Get all mondays of iterations over weeks of size n
    # nDictTraining[n]['monday'][0]: Get the 1st (AKA the 0th) monday of the mondays of iterations. Paired list [date, ordercounts]
    # nDictTraining[n]['monday'][0][0]: Get the date of the 1st (AKA the 0th) monday of iterations

    for x in range(init, n + 1):
        training = {names: [] for names in headers}
        test = {}
        for weekdayArray, names in zip(enumOrdersList, headers):
            for i in range(len(weekdayArray) - x):
                training[names].append([weekdayArray[i + j] for j in range(x)])
        for keys in training:
            test[keys] = training[keys][-b:]
        for keys in training:
            training[keys] = training[keys][:len(training[keys]) - b]
        nDictTraining[x] = training
        nDictTest[x] = test

    # regressions: list of predicted order counts
    resultset = regression(n,df,nDictTraining,nDictTest,init,headers)

    regressions = []
    for x in [1, 2]:
        for day in headers:
            regressions.append(resultset[day][2](x))

    if selfAccuracyAnalysis:
        mode = "ACCURACY"
        graphFilename = outputPath + "/accuracyGraph" + created + "." + graphFileType
        csvFilename = outputPath + '/accuracyResults' + created + '.csv'
        # initDate in Accuracy mode is the first day of the last 2 weeks worth of true data.
        initDate = trueData[0][0]

        # offset is the number of days initDate is different from the monday of the corresponding week.
        # E.g. Tuesday == 1, Sunday == 6.
        offset = initDate.weekday()

        # trueDates: 2 weeks worth of true dates.
        # trueCounts: 2 weeks worth of true orders.
        trueDates, trueCounts = zip(*trueData)

    else:
        mode = "PREDICTION"
        graphFilename = outputPath + "/predictionGraph" + created + "." + graphFileType
        csvFilename = outputPath + '/predictionResults' + created + '.csv'
        # initDate in Prediction mode is the last day of the week of all the data we have.
        initDate = trueData[-1:][0][0]

        # Same as Accuracy mode
        offset = initDate.weekday()

        # futureDates is two weeks worth of consecutive days starting at initDate.
        futureDates = []
        for i in range(14):
            futureDates.append(initDate + dt.timedelta(days=i))

    # List regressions is: [1st Monday, ..., 1st Sunday, 2nd Monday, ..., 2nd Sunday]
    # Want to order regressions by date, which looks like:
    # [1st Nth-day, ..., 1st N+6th-day, 2nd Nth-day, ..., 2nd N+6th Day]
    regressions = regressions[:7][offset:] + regressions[:7][:offset] + regressions[7:][offset:] + regressions[7:][
                                                                                                   :offset]

    if importantDateFlag:
        print("\nExcluded Days (Special Days):")
        for items in importantData:
            print(items)
    print("\n################### RESULTS BELOW: MODE = ", mode, "########################\n")

    if selfAccuracyAnalysis:
        # Print and save Accuracy Data:
        print("True Data:")
        print(trueData)
        print("\nPredicted Data:")
        print(list(zip(trueDates, map(int, map(round, regressions)))), "\n")
        # This block of code below just organizes the data so it prints nicely.
        if regressionOut:
            with open(outputPath + "/AccuracyModel" + timestr + ".txt", "w") as textFile:
                for day in headers:
                    printAdder = ""
                    if resultset[day][1] > 1:
                        coeffSet = list(resultset[day][2])[::-1]
                        printAdder += "\n"
                        for i, m in reversed(list(enumerate(coeffSet))):
                            if i > 1:
                                printAdder += str(m) + " x^" + str(i) + " + "
                            elif i == 1:
                                printAdder += str(m) + " x" + " + "
                            else:
                                printAdder += str(m)
                    else:
                        printAdder = str(resultset[day][2])
                        # Writes regression model to a text file
                    textFile.write(
                        day + "\nregression model: " + printAdder + "\nerror = " + str(resultset[day][3]) + "\n\n")
                    print("\n" + day, "\nregression model:", printAdder, "\nerror = ", resultset[day][3])

        # Accuracy graph is being produced and saved to a file here
        if regressionGraph:
            plt.figure(10)
            plt.plot(trueDates, regressions, 'r', label="Accuracy")
            plt.plot(trueDates, trueCounts, 'b', label="True")
            plt.xticks(rotation=90)
            if regressionGraph:
                plt.savefig(graphFilename)

        # Accuracy data is being produced and saved to a .csv here
        if regressionCSV:
            with open(csvFilename, 'w', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(["Date", "Predicted Value", "True Value"])
                writer.writerows(zip(list(trueDates), list(regressions), list(trueCounts)))

    else:
        # Print and save Prediction Data:
        print("Predicted Data:")
        print(list(zip(futureDates, map(int, map(round, regressions)))), "\n")
        # Same as Accuracy Mode
        if regressionOut:
            with open(outputPath + "/PredictionModel" + timestr + ".txt", "w") as textFile:
                for day in headers:
                    printAdder = ""
                    if resultset[day][1] > 1:
                        coeffSet = list(resultset[day][2])[::-1]
                        printAdder += "\n"
                        for i, m in reversed(list(enumerate(coeffSet))):
                            if i > 1:
                                printAdder += str(m) + " x^" + str(i) + " + "
                            elif i == 1:
                                printAdder += str(m) + " x" + " + "
                            else:
                                printAdder += str(m)
                    else:
                        printAdder = str(resultset[day][2])
                    # Writes regression model to a text file
                    textFile.write(
                        day + "\nregression model: " + printAdder + "\nerror = " + str(resultset[day][3]) + "\n\n")
                    print("\n" +
                          day, "\nregression model:", printAdder, "\nerror = ", resultset[day][3])
        # Saves regression graph
        if regressionGraph:
            plt.figure(10)
            plt.plot(futureDates, regressions, 'r', label="Prediction")
            if regressionGraph:
                plt.savefig(graphFilename)

        # Saves regression data csv
        if regressionCSV:
            with open(csvFilename, 'w', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(["Date", "Predicted Value"])
                writer.writerows(zip(list(futureDates), list(regressions)))

    wkdayflag = [hasMonday,
                 hasTuesday,
                 hasWednesday,
                 hasThursday,
                 hasFriday,
                 hasSaturday,
                 hasSunday][::-1]

    # Saves to <Insert-Weekday-Here>OrderCounts.csv
    for x, days in zip(dataFrame, headers):
        if wkdayflag.pop():
            x[['actualStart', 'deliveryCount']].to_csv(outputPath + '/' + days + 'OrderCounts' + created + '.csv')
    return resultset

if __name__ == "__main__":
    if shiftCountCSV or shiftCountGraph:
        plot_shiftcounts(1, outputPath + '/shift_counts' + created + '.' + graphFileType)

    if orderCountCSV or orderCountGraph:
        plot_ordercounts(2, outputPath + '/order_counts_obs' + created + '.' + graphFileType)

    if regressionCSV or regressionGraph:
        perform_analysis()

import unittest

if __name__ == '__main__':
    class myTest(unittest.TestCase):
        def test_reset(self):
            di.reset_cache()
            self.assertIsNone(di.id)
            self.assertIsNone(di.rs)
            self.assertIsNone(di.rw)
            self.assertIsNone(di.sl)
            self.assertIsNone(di.tw)

        def test_execution(self):
            if len(sys.argv) > 1:
                self.assertEquals(len(sys.argv), 21)
            else:
                self.assertEqual(len(paramList), 22)

        def test_storeFilter(self):
            # Test the order counts of 153 and 181 so that they are equal to Andrew's output
            pass

        def test_linear_regression(self):
            # Test the order counts of 153 and 181 so that they are equal to Andrew's output
            resultset = perform_analysis("../Inputs/test_linear.csv")
            for day in resultset:
                self.assertLessEqual(resultset[day][3], 10e-5)

        def test_constant_regression(self):
            resultset = perform_analysis("../Inputs/test_constant.csv")
            for day in resultset:
                self.assertLessEqual(resultset[day][3], 10e-5)

# Only run unit tests if you run the system through the UI.
if len(sys.argv) == 0:
    unittest.main()
